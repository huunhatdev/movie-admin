import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button, message, Tag } from "antd";
import { userServices } from "../pages/services/userService";

export const headerTableUser = [
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
    render: (text) => {
      return <span className="text-blue-500">{text}</span>;
    },
  },
  {
    title: "Họ tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Số điện thoại",
    dataIndex: "soDT",
    key: "soDT",
  },
  {
    title: "Mật khẩu",
    dataIndex: "matKhau",
    key: "matKhau",
  },
  {
    title: "Loại người dùng",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    render: (text) => {
      let color = text.toUpperCase() === "KHACHHANG" ? "blue" : "green";
      let type = text.toUpperCase() === "KHACHHANG" ? "Khách hàng" : "Quản trị";
      return (
        <Tag color={color} key={text}>
          {type}
        </Tag>
      );
    },
  },
  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
    render: (action, record) => {
      return (
        <div className="space-x-2">
          <Button type="default">
            <EditOutlined />
          </Button>
          <Button
            type="default"
            danger
            onClick={() => {
              action.onDelete();
            }}
          >
            <DeleteOutlined />
          </Button>
        </div>
      );
    },
  },
];

import { configureStore } from "@reduxjs/toolkit";
import spinnerSlice from "./slices/spinnerSlice";
import userSlice from "./slices/userSlice";

export const store = configureStore({
  reducer: {
    userSlice,
    spinnerSlice,
  },
});

import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  value: 0,
};

const demoSlice = createSlice({
  name: "demoSlice",
  initialState,
  reducers: {
    increment: (state, action) => {
      state.value += action.payload;
    },
    decrement: (state, action) => {
      state.value -= action.payload;
    },
  },
});

export const { increment, decrement } = demoSlice.actions;

export default demoSlice.reducer;

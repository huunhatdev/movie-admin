import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { userServices } from "../../pages/services/userService";

let initialState = {
  user: null,
  loading: false,
};

export const setUserLoginActionServ = createAsyncThunk(
  "/userSlice/login",
  async (dataLogin) => {
    let result = await userServices.postDangNhap(dataLogin);
    return result.data.content;
  }
);

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserLogin: (state, { payload }) => {
      state.user = payload;
    },
  },

  extraReducers: {
    //pending
    [setUserLoginActionServ.pending]: (state, action) => {
      state.loading = true;
    },
    //success
    [setUserLoginActionServ.fulfilled]: (state, action) => {
      state.loading = false;
      state.user = action.payload;
    },
    //fail
    [setUserLoginActionServ.rejected]: (state, action) => {},
  },
});

export default userSlice.reducer;

export const { setUserLogin } = userSlice.actions;

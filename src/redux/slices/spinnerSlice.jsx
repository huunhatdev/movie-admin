import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  isLoading: false,
};

const spinnerSlice = createSlice({
  name: "loadingSlice",
  initialState,
  reducers: {
    setSpinner: (state, { type, payload }) => {
      state.isLoading = payload;
    },
  },
});

export const { setSpinner } = spinnerSlice.actions;

export default spinnerSlice.reducer;

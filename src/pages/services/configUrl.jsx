import axios from "axios";
import { setSpinner } from "../../redux/slices/spinnerSlice";
import { store } from "../../redux/store";
import { localStorageService } from "./localStorageService";

export const BASE_URL = "https://movienew.cybersoft.edu.vn";
export const TOKEN_CYBERSOFT = "ss";
// "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwMyIsIkhldEhhblN0cmluZyI6IjAxLzAxLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3MjUzMTIwMDAwMCIsIm5iZiI6MTY0NzUzNjQwMCwiZXhwIjoxNjcyNjc4ODAwfQ.v1pky9yKwnujpoxePbaS26rxq_cGpKrk0GvA0sHAVqY";

export const httpService = axios.create({
  baseURL: BASE_URL,
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: "Bearer " + localStorageService.getUserInfo()?.accessToken,
  },
});

httpService.interceptors.request.use(
  function (config) {
    store.dispatch(setSpinner(true));
    return config;
  },
  function (err) {
    store.dispatch(setSpinner(false));
    return Promise.reject(err);
  }
);
httpService.interceptors.response.use(
  function (config) {
    store.dispatch(setSpinner(false));
    return config;
  },
  function (err) {
    store.dispatch(setSpinner(false));
    console.log(err);
    let status = err.response.status;
    switch (status) {
      case 401:
      case 403:
        window.location.href = "/login";
    }
    return Promise.reject(err);
  }
);

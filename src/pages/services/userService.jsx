import axios from "axios";
import { BASE_URL, httpService, TOKEN_CYBERSOFT } from "./configUrl";
import { localStorageService } from "./localStorageService";

export const userServices = {
  postDangNhap: (dataLogin) => {
    return axios({
      method: "POST",
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      data: dataLogin,
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    });
  },
  postDangKi: (dataSignUp) => {
    return axios({
      method: "POST",
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
      data: { ...dataSignUp, maNhom: 2 },
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    });
  },
  getUserList: () =>
    httpService.get(`/api/QuanLyNguoiDung/LayDanhSachNguoiDung`),
  deleteUser: (taiKhoan) => {
    return httpService.delete(
      `/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`
    );
  },
};

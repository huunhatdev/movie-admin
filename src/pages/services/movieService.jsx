import axios from "axios";
import { BASE_URL, TOKEN_CYBERSOFT } from "./configUrl";

export const movieService = {
  getMovieList: (maNhom = "GP01") => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=${maNhom}`,
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    });
  },

  getMovieByTheater: (maNhom = "GP01") => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=${maNhom}`,
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    });
  },

  getDetailMovie: (MaPhim) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${MaPhim}`,
      method: "GET",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    });
  },
};

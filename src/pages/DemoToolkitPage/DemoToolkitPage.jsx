import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { decrement, increment } from "../../redux/slices/demoSlice";

export default function DemoToolkitPage() {
  const value = useSelector((state) => state.demoSlice.value);
  const dispatch = useDispatch();
  return (
    <div className=" container mx-auto py-40">
      <button
        className="bg-green-500 p-4 rounded mx-2"
        onClick={() => dispatch(decrement(1))}
      >
        -
      </button>
      <span className="text-2xl text-pink-600">{value}</span>
      <button
        className="bg-red-500 p-4 rounded mx-2"
        onClick={() => dispatch(increment(1))}
      >
        +
      </button>
    </div>
  );
}

import React, { useCallback, useEffect, useState } from "react";
import { message, Space, Table, Tag } from "antd";
import { userServices } from "../services/userService";
import { headerTableUser } from "../../Utils/userManagement copy";
import useWindowSize from "../../Hook/useWindowSize";
import HeaderResponsive from "../../components/Header/HeaderResponsive";
import SpinnerComponent from "../../components/SpinnerComponent/SpinnerComponent";
import { useDispatch, useSelector } from "react-redux";
import { setSpinner } from "../../redux/slices/spinnerSlice";

export default function UserManagePage() {
  const [users, setUsers] = useState([]);

  let fetchUsers = () => {
    userServices
      .getUserList()
      .then((res) => {
        let dataClone = res.data.content.map((e, i) => {
          return {
            ...e,
            action: {
              onDelete: () => {
                userServices
                  .deleteUser(e.taiKhoan)
                  .then((res) => {
                    message.success(res.data.content);
                    fetchUsers();
                  })
                  .catch((err) => {
                    message.error(err.response.data.content);
                  });
              },
              onEdit: () => {},
            },
          };
        });
        setUsers(dataClone);
      })
      .catch((err) => {});
  };
  useEffect(() => {
    fetchUsers();
  }, []);

  let windowSize = useWindowSize();

  return (
    <>
      <div>
        {/* {isLoading && <SpinnerComponent />} */}
        <HeaderResponsive />
      </div>
      <div className="container mx-auto pt-10">
        <Table columns={headerTableUser} dataSource={users} />
      </div>
    </>
  );
}

import { Button, Checkbox, Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  setUserLogin,
  setUserLoginActionServ,
} from "../../redux/slices/userSlice";
import { localStorageService } from "../services/localStorageService";
import { userServices } from "../services/userService";

const LoginPage = () => {
  let navigate = useNavigate();

  let dispatch = useDispatch();

  const onFinish = (values) => {
    //unwrap ~ handle dispatch thành promise
    dispatch(setUserLoginActionServ(values))
      .unwrap()
      .then((res) => {
        if (res.maLoaiNguoiDung === "QuanTri") {
          localStorageService.setUserInfo(res);
          navigate("/");
        } else {
          message.error("Không đủ quyền truy cập");
        }
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="container mx-auto pt-40">
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="matKhau"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default LoginPage;

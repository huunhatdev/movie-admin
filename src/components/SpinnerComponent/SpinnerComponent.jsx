import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { ScaleLoader } from "react-spinners";

export default function SpinnerComponent() {
  const { isLoading } = useSelector((state) => state.spinnerSlice);

  const dispatch = useDispatch();
  return isLoading ? (
    <div className="fixed h-screen w-screen bg-black/50 z-50 flex justify-center items-center">
      <ScaleLoader color="#FF7396" />
    </div>
  ) : (
    <></>
  );
}

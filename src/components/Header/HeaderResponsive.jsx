import React from "react";
import useWindowSize from "../../Hook/useWindowSize";
import HeaderDesktop from "./HeaderDesktop";
import HeaderMobile from "./HeaderMobile";
import HeaderTablet from "./HeaderTablet";

export default function HeaderResponsive() {
  let windowSize = useWindowSize();

  let renderHeader = () => {
    if (windowSize.width > 992) return <HeaderDesktop />;
    if (windowSize.width > 768) return <HeaderTablet />;
    return <HeaderMobile />;
  };
  return <div>{renderHeader()}</div>;
}
